#!/bin/bash

# 引数の確認
if [ -z "$1" ] || [ -z "$2" ]; then
  echo "Usage: $0 <duration> <Up|Down>"
  exit 1
fi

# 引数で受け取った期間
DURATION=$1

# 引数で受け取ったUpまたはDown
UP_DOWN=$2

# PrometheusのURLとクエリ
PROMETHEUS_URL="http://a5c794d9812c1464081dec285ddb1a9d-1585132098.ap-northeast-1.elb.amazonaws.com:9090"
GET_UPTIME="sum_over_time((probe_success{job='prom_blackbox'})[${DURATION}:])"
GET_DOWNTIME="sum_over_time((1 - probe_success{job='prom_blackbox'})[${DURATION}:])"

# 現在の日付を取得
CURRENT_DATE=$(date +"%Y-%m-%d")

# 引数に応じて選択されたクエリを使用
if [ "$UP_DOWN" == "Up" ]; then
  QUERY=$GET_UPTIME
elif [ "$UP_DOWN" == "Down" ]; then
  QUERY=$GET_DOWNTIME
else
  echo "Invalid second argument. Use 'Up' or 'Down'."
  exit 1
fi

# curlでクエリを実行し、結果を日付を含むファイル名で保存
curl -G "${PROMETHEUS_URL}/api/v1/query" --data-urlencode "query=${QUERY}" > "output_${CURRENT_DATE}.json"

