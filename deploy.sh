#!/bin/bash

kubectl apply --server-side -f manifests/

sleep 30
# Safety wait for resources to be created

# kubectl rollout status -n default daemonset node-exporter
# kubectl rollout status -n default statefulset alertmanager-main
# kubectl rollout status -n default statefulset prometheus-k8s
# kubectl rollout status -n default deployment grafana
# kubectl rollout status -n default deployment kube-state-metrics

