kubectl port-forward -n default svc/grafana 3000 > /dev/null 2>&1 &
# kubectl port-forward -n default svc/alertmanager 9093 > /dev/null 2>&1 &
kubectl port-forward -n default svc/prometheus 9090 > /dev/null 2>&1 &
