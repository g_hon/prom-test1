git clone https://github.com/TakumaNakagame/prometheus-book.git

#### kind

×：https://qiita.com/sourjp/items/281d2189516823950291
〇：https://tech-lab.sios.jp/archives/19226

https://info.drobe.co.jp/blog/engineering/docker-no-space-left-on-device

docker system prune -a --volumes


# Prometheus-Operator

kube-prometheus

https://github.com/prometheus-operator/kube-prometheus
https://github.com/prometheus-operator/prometheus-operator/blob/master/Documentation/api.md


https://kun432.hatenablog.com/entry/kube-prometheus-1

```
kubectl --namespace monitoring port-forward svc/prometheus-k8s 9090
kubectl --namespace monitoring port-forward svc/grafana 3000
kubectl --namespace monitoring port-forward svc/alertmanager-main 9093
```

http://localhost:9090
http://localhost:3000
http://localhost:9093/#/alerts

```
kubectl -n monitoring patch service prometheus-k8s -p '{"spec":{"type": "NodePort"}}'
kubectl -n monitoring patch service prometheus-k8s --type='json' -p='[{"op": "replace", "path": "/spec/ports/0/nodePort", "value": 30080}]'
kubectl -n monitoring patch service alertmanager-main -p '{"spec":{"type": "NodePort"}}'
kubectl -n monitoring patch service alertmanager-main --type='json' -p='[{"op": "replace", "path": "/spec/ports/0/nodePort", "value": 30081}]'
kubectl -n monitoring patch service grafana -p '{"spec":{"type": "NodePort"}}'
kubectl -n monitoring patch service grafana --type='json' -p='[{"op": "replace", "path": "/spec/ports/0/nodePort", "value": 30082}]'
```



##### [kube-prometheus はサポートするバージョンの対応表](https://fand.jp/technologies/how-to-install-prometheus-operator-with-helm/)

##### [Prometheus、Alertmanager、Grafana への外部アクセス用に LoadBalancer サービスを使用](https://hassiweb.gitlab.io/memo/docs/memo/k3s/prometheus-operator/)

#### [alertmanager email](https://geekdudes.wordpress.com/2020/03/24/kubernetes-prometheus-operator-email-notification-configuration/)



### GrafanaLoki + AlertManager

- https://www.ogis-ri.co.jp/otc/hiroba/technical/kubernetes_use/part5.html

- https://www.scsk.jp/sp/sysdig/blog/prometheus/prometheuskubernetes-_prometheus_operator_3.html

### helm

- https://fand.jp/technologies/how-to-install-prometheus-operator-with-helm/





---

##### [Kubernetes における prometheus, alertmanager, grafana の連携](https://zenn.dev/empenguin/articles/c9f3ca5084b498)

##### [GrafanaをKubernetesで実際に運用してみる](https://qiita.com/MetricFire/items/f355a93107e713171569)

```
kubectl create deployment grafana --image=grafana/grafana:8.3.6  --namespace=default --dry-run=client -o yaml
kubectl expose deployment grafana --type=LoadBalancer --port=80 --target-port=3000 --protocol=TCP
```

##### [HTTPレスポンス速度](https://handon.hatenablog.jp/entry/2019/01/29/005935)

### 

```
echo -n 'admin' | base64
```



```
kubectl create secret generic mysecret --from-file=data.csv=./data.csv
```

```
kubectl create secret generic alertmanager --from-file=alertmanager.yaml=./alertmanager.yaml --dry-run=client -o yaml
```



---



# 参考

https://qiita.com/mttk030/items/9972d2548f987f9f9563

### blackbox_exporter

使用Port：9115
HTTPやTCPなどを介したエンドポイント情報を送信。Webサービスの死活監視に。
おすすめDashboard：https://grafana.com/grafana/dashboards/7587

#### Mattermostのパフォーマンス監視

https://docs.mattermost.com/scale/performance-monitoring.html

#### Prometheus公式ドキュメント

https://prometheus.io/docs/prometheus/latest/getting_started/#

#### kubectl コマンド一覧

https://cstoku.dev/posts/2018/k8sdojo-23/

#### Kubernetesのサービスで利用するClusterIPとNodePortの違い

https://www.mtioutput.com/entry/k8s-nodeport-handson

#### Mattermost on kubernetes

https://github.com/mattermost/mattermost-docker/tree/master/contrib/kubernetes

  - kubectl create deployment postgres --image=postgres:9

##### [PrometheusとGrafanaによるKubernetesクラスタのモニタリングとアラート機能（前半）  ](https://gavin-zhou.medium.com/prometheus%E3%81%A8grafana%E3%81%AB%E3%82%88%E3%82%8Bkubernetes%E3%82%AF%E3%83%A9%E3%82%B9%E3%82%BF%E3%81%AE%E3%83%A2%E3%83%8B%E3%82%BF%E3%83%AA%E3%83%B3%E3%82%B0%E3%81%A8%E3%82%A2%E3%83%A9%E3%83%BC%E3%83%88%E6%A9%9F%E8%83%BD-%E5%89%8D%E5%8D%8A-2cd62ba32614)

##### [【Prometheus + Grafana】Kubernetesを監視するためのセットアップを１から解説](https://qiita.com/MetricFire/items/1f15b6f1237ade0ce0d9)


###### alertmanager gmail
NG:https://geekdudes.wordpress.com/2020/03/24/kubernetes-prometheus-operator-email-notification-configuration/
NG：https://uzimihsr.github.io/post/2020-01-27-alertmanager-gmail/

OK：https://grafana.com/blog/2020/02/25/step-by-step-guide-to-setting-up-prometheus-alertmanager-with-slack-pagerduty-and-gmail/


##### 参考（構築）
https://github.com/giantswarm/prometheus
https://github.com/kayrus/prometheus-kubernetes


##### 参考（アラート）
[Prometheus 監視をまるっと設定](https://www.techscore.com/blog/2017/12/07/prometheus-monitoring-setting/)

##### mattermost
- [Prometheus AlertManager プラグイン](https://github.com/cpanato/mattermost-plugin-alertmanager/blob/main/README.md)
  - Prometheus AlertManager から Mattermost に通知を送信する
- https://developers.mattermost.com/blog/streamlining-developer-access-to-prometheus-and-grafana/


---

exporterの一覧（prometheus公式ドキュメント）
https://prometheus.io/docs/instrumenting/exporters/


##### 参考

https://zenn.dev/daiskoba/scraps/da76e0d8df6cba