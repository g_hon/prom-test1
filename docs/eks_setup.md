## [eksctl と kubectl の導入](https://catalog.us-east-1.prod.workshops.aws/workshops/f5abb693-2d87-43b5-a439-77454f28e2e7/ja-JP/020-create-cluster/10-install-eksctl#eksctl)
この節では、Kubernetes クラスター自体の作成に使用する eksctl  コマンドと、作成した Kubernetes クラスターの操作に使用する kubectl コマンドをインストールします。

### eksctl
eksctl コマンドをインストールします。

```
curl -L "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
sudo mv /tmp/eksctl /usr/local/bin

```

eksctl コマンドのバージョンを確認しましょう。0.24.0 以上のバージョンを使用して下さい。

```eksctl version```

### kubectl
kubectl コマンドは EKS クラスターの Kubernetes バージョンに合わせたバージョンを使用します。各バージョンのダウンロード URL は以下のリンク先を確認して下さい。

[kubectl のインストール ](https://docs.aws.amazon.com/ja_jp/eks/latest/userguide/install-kubectl.html)
このハンズオンでは Kubernetes のバージョンは 1.24 を使用しますので、同じバージョンの kubectl コマンドをインストールします。

```
sudo curl -L -o /usr/local/bin/kubectl https://s3.us-west-2.amazonaws.com/amazon-eks/1.25.9/2023-05-11/bin/linux/amd64/kubectl
sudo chmod +x /usr/local/bin/kubectl

```

kubectl コマンドのバージョンを確認しましょう。クライアント側のみのバージョンを確認する場合は --client フラグを使用します。

```
kubectl version --short --client

```



```
eksctl create cluster --name eks-test --region ap-northeast-1 --node-type t3.medium --nodes 3 --nodes-min 3 --nodes-max 3
```



```
eksctl delete cluster --name eks-test
```

