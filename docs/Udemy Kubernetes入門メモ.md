## EKS


### eksctlのインストール
https://github.com/weaveworks/eksctl/blob/main/README.md#installation

#### Git Bash の使用:


```
# for ARM systems, set ARCH to: `arm64`, `armv6` or `armv7`
ARCH=amd64
PLATFORM=windows_$ARCH

curl -sLO "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$PLATFORM.zip"

# (Optional) Verify checksum
curl -sL "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_checksums.txt" | grep $PLATFORM | sha256sum --check

unzip eksctl_$PLATFORM.zip -d $HOME/bin

rm eksctl_$PLATFORM.zip
```

```
eksctl create cluster --name test-cluster --region ap-northeast-1  --profile default
aws eks update-kubeconfig --name test-cluster  --profile default
kubectl get node

kubectl create service loadbalancer prometheus --tcp=9090 --dry-run=client -o yaml
kubectl create service loadbalancer prometheus --tcp=9090

eksctl delete cluster --name test-cluster --region ap-northeast-1  --profile default
```