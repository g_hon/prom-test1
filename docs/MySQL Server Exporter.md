# MySQL Server Exporter



### 記事

- [Kubernetes環境でMySQL用Exporterからのメトリクス取得](https://qiita.com/rjegg/items/6b423d920c81409d855b)
- [Using Prometheus to Monitor MySQL and MariaDB](https://www.tencentcloud.com/document/product/457/38553)
- [MySQL Server Exporterを使って、Prometheus × Grafanaでモニタリングする](https://kazuhira-r.hatenablog.com/entry/2018/12/20/003535)
- [Monitoring MySQL using Prometheus, Grafana and mysqld_exporter in Kubernetes](https://blog.devops.dev/monitoring-mysql-using-prometheus-and-grafana-in-kubernetes-16e7ae3de5dd)
- https://exporterhub.io/exporter/mysql-exporter/



### GitHub

- https://github.com/prometheus/mysqld_exporter



### Grafana

- [MySQL Overview](https://grafana.com/grafana/dashboards/7362-mysql-overview/)
- [MySQL Exporter Quickstart and Dashboard](https://grafana.com/grafana/dashboards/14057-mysql/)
- [Mysql - Prometheus](https://grafana.com/grafana/dashboards/6239-mysql/)
- https://github.com/percona/grafana-dashboards/tree/main/dashboards/MySQL
- https://github.com/percona/grafana-dashboards/tree/v1.17.0/dashboards



### Port-forward

- [[Kubernetes] クラスタ外から ClusterIP の Service にいい感じにアクセスする](https://qiita.com/superbrothers/items/0dca5d2a10727fc14734)
- [kubectl port-forwardでクラスタ外からPodにアクセスする](https://udomomo.hatenablog.com/entry/2020/11/01/235612)